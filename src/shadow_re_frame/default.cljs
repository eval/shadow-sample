(ns shadow-re-frame.default
  (:require
    [re-frame.core :as rf]
    [reagent.core :as reagent]

    #_[day8.re-frame.trace.db :as trace-db]))



;; 1. Event Dispatch
;;    make a view, dispatch an event in a click handler

(defn counter
  "Given a counter id and its current value, render it as an interactive widget."
  [id]
  (let [counter-value @(rf/subscribe [::counter id])]
    [:div.counter {:style {:display :grid :grid-template-columns "80% 20%"}}
     [:div {:on-click #(rf/dispatch [:inc-counter id])
            :style    {:padding    20
                       :margin     "10px 10px"
                       :background "rgba(0,0,0,0.05)"
                       :cursor     "pointer"}}
      (str "Counter " (name id) ": ")
      counter-value]
     [:a {:href     "#"
          :on-click #(rf/dispatch [:remove-counter id])
          :style    {:padding "20px 0" :margin "10px 0"}} "🗑"]]))


;; 2. Event Handling
;;    register a handler for a given event.
;;
;;    - handlers are identified by keyword.
;;    - simple method:   `reg-event-db` is passed `db` as 1st argument.
;;      advanced method: `reg-event-fx` is passed 'co-effects' map as 1st argument of which `:db` is one key.

(rf/reg-event-db :remove-counter
                 (fn [db [_ counter-id]]
                   (update-in db [::counters] assoc counter-id nil)))

(rf/reg-event-db :inc-counter
                 (fn [db [_ counter-id]]
                   (update-in db [::counters counter-id] inc)))


(rf/reg-event-db :initialize
                 ;; we'll call this once, at the beginning, to set up the db.
                 (constantly {::counters {"A" 0
                                          "B" 0
                                          "C" 0}}))


(rf/reg-event-fx :add-counter
                 (fn [{{counters ::counters :as db} :db :as coeffects} event]
                   (let [counter-options (re-seq #"." "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
                         next-counter    (nth counter-options (count counters))]
                     (prn :coeffects coeffects :event event :counters counters)
                     {:db (update db ::counters assoc next-counter 0)})))

(comment
  (update-in {:db {:counters {"A" 1}}} [:db :counters] assoc "B" 2))

;; 3. Queries
;;    make a query for every kind of 'read' into the db.
;;
;;    - queries are identified by keyword.
;;    - queries can (optionally) take parameters.
;;    - `db` is passed as 1st arg to function.
;;      vector of [query-id & args] is passed as 2nd arg.

(rf/reg-sub ::counter
            (fn [db [_ counter-id]]
              (get-in db [::counters counter-id])))

(rf/reg-sub ::counter-ids
            (fn [db _]
              (as-> db $
                  (get $ ::counters)
                  (filter (comp some? val) $)
                  (keys $))))

;; 4. Views
;;    Use reagent to create views.
;;
;;    - use `re-frame.core/subscribe` to read queries
;;    - use `reagent/atom` for local state (not shown here)

(defn root-view
  "Render the page"
  []
  (fn []
    [:div
     {:style {:max-width 300
              :margin    "50px auto"
              :font-size 16}}
     "Click to count!"
     (let [counters @(rf/subscribe [::counter-ids])]
       (doall (for [id counters]
                ^{:key id} [counter id])))
     [:a {:href     "#"
          :on-click #(rf/dispatch [:add-counter])}
      "Add counter"]]))

(defn render
  []
  (reagent/render [root-view]
                  (js/document.getElementById "shadow-re-frame")))


(defn ^:dev/after-load clear-cache-and-render! []
  (rf/clear-subscription-cache!)
  (render))


(defn init []
  (rf/dispatch [:initialize])
  (render))
